FROM php:fpm


RUN docker-php-ext-install pdo pdo_mysql \
  && docker-php-ext-enable pdo pdo_mysql \
  && kill -USR2 1
