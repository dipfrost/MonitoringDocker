-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: mydb
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.16.04.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `containers`
--

DROP TABLE IF EXISTS `containers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `containers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nodename` varchar(150) DEFAULT NULL,
  `dockername` varchar(70) DEFAULT NULL,
  `blocks` varchar(150) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `version` varchar(150) DEFAULT NULL,
  `vin` varchar(255) DEFAULT NULL,
  `active_status` varchar(50) DEFAULT NULL,
  `timestamp` varchar(40) DEFAULT NULL,
  `reindex` varchar(40) DEFAULT NULL,
  `locate` varchar(40) DEFAULT NULL,
  `nodeid` varchar(150) DEFAULT NULL,
  `dockernumber` varchar(50) NOT NULL DEFAULT '0',
  `genkey` varchar(200) NOT NULL DEFAULT '',
  `delcontainer` int(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=711 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `containers`
--

LOCK TABLES `containers` WRITE;
/*!40000 ALTER TABLE `containers` DISABLE KEYS */;
INSERT INTO `containers` VALUES (700,'Advance','896da2c57745','  \"blocks\": 317846,\r','  \"status\": \"Node just started, not yet activated\"\r','  \"version\": 130100,\r','  \"outpoint\": \"0000000000000000000000000000000000000000000000000000000000000000-4294967295\",\r','4','16:04:07 2018-11-02 ','0','rivas-X299-UD4',NULL,'11','kBvwoGR5F8PRmozTtP9jtgi7oZYQaxGiQfHUzwbivq7KPEUiir',0),(701,'Advance','7e640ebb60a3','  \"blocks\": 317846,\r','  \"status\": \"Node just started, not yet activated\"\r','  \"version\": 130100,\r','  \"outpoint\": \"0000000000000000000000000000000000000000000000000000000000000000-4294967295\",\r','4','16:04:08 2018-11-02 ','0','rivas-X299-UD4',NULL,'10','k8UDWfXbtzKFVxfvbY116mkhwn3WiQPx9nrNhVWbvb54TeQfhH',0),(703,'Advance','57907adb9ce2','  \"blocks\": 317846,\r','  \"status\": \"Node just started, not yet activated\"\r','  \"version\": 130100,\r','  \"outpoint\": \"0000000000000000000000000000000000000000000000000000000000000000-4294967295\",\r','4','16:04:09 2018-11-02 ','0','rivas-X299-UD4',NULL,'8','kyijHHpj9m4dRGSfnbaPjWsXyDrsTdYStrMXuZHXCrVEXomAfm',0),(704,'Advance','56fc9492edd3','  \"blocks\": 317846,\r','  \"status\": \"Node just started, not yet activated\"\r','  \"version\": 130100,\r','  \"outpoint\": \"0000000000000000000000000000000000000000000000000000000000000000-4294967295\",\r','4','16:04:09 2018-11-02 ','0','rivas-X299-UD4',NULL,'7','jp1FofSdNAJFdDAkHSCt66kGaWcycEmdLRL5ZhF8HtKrfMfi69',0),(705,'Advance','528c7e709d3d','  \"blocks\": 317846,\r','  \"status\": \"Node just started, not yet activated\"\r','  \"version\": 130100,\r','  \"outpoint\": \"0000000000000000000000000000000000000000000000000000000000000000-4294967295\",\r','4','16:04:10 2018-11-02 ','0','rivas-X299-UD4',NULL,'6','k6g4Ph1bPzQQsTYthmVqcJXs4u94rH4derpv9SLUnAyKSxbyJL',0),(706,'Advance','c9bad6bce926','  \"blocks\": 317846,\r','  \"status\": \"Node just started, not yet activated\"\r','  \"version\": 130100,\r','  \"outpoint\": \"0000000000000000000000000000000000000000000000000000000000000000-4294967295\",\r','4','16:04:10 2018-11-02 ','0','rivas-X299-UD4',NULL,'5','knWT8kCyBX6hobqpaishnhcwc42uzDQQUzNzXsQt9Yq6Q7cd65',0),(707,'Advance','0e76e4de777b','  \"blocks\": 317846,\r','  \"status\": \"Node just started, not yet activated\"\r','  \"version\": 130100,\r','  \"outpoint\": \"0000000000000000000000000000000000000000000000000000000000000000-4294967295\",\r','4','16:04:10 2018-11-02 ','0','rivas-X299-UD4',NULL,'4','mYsdwMZNz3kEts4goU3qrn4GRQ7PBwUGaZqtsjKpGGa3eKYr5W',0),(708,'Advance','caee45cbd650','  \"blocks\": 317846,\r','  \"status\": \"Node just started, not yet activated\"\r','  \"version\": 130100,\r','  \"outpoint\": \"0000000000000000000000000000000000000000000000000000000000000000-4294967295\",\r','4','16:04:11 2018-11-02 ','0','rivas-X299-UD4',NULL,'3','knzm9GhxXb1HqpUWRw6KbJcHsPAgrpTHQAwLkEGnLh5smJtdUP',0),(709,'Advance','770dd88122c5','  \"blocks\": 317846,\r','  \"status\": \"Node just started, not yet activated\"\r','  \"version\": 130100,\r','  \"outpoint\": \"0000000000000000000000000000000000000000000000000000000000000000-4294967295\",\r','4','16:04:11 2018-11-02 ','0','rivas-X299-UD4',NULL,'2','kL6Rj6FZnmb26aujUtr22g9KDydNYchY9AMBUNw1Ms1eiaGbbQ',0),(710,'Advance','71363f26498b','  \"blocks\": 317846,\r','  \"status\": \"Node just started, not yet activated\"\r','  \"version\": 130100,\r','  \"outpoint\": \"0000000000000000000000000000000000000000000000000000000000000000-4294967295\",\r','4','16:04:12 2018-11-02 ','0','rivas-X299-UD4',NULL,'1','kxTTgyFc4i5XPReGhschS2XPKyFGbfWsJCRj2uw2ZD1U6KTepd',0);
/*!40000 ALTER TABLE `containers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `docker`
--

DROP TABLE IF EXISTS `docker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(100) NOT NULL,
  `tag` varchar(50) NOT NULL DEFAULT '',
  `locate` varchar(100) NOT NULL DEFAULT '',
  `imageid` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `docker`
--

LOCK TABLES `docker` WRITE;
/*!40000 ALTER TABLE `docker` DISABLE KEYS */;
INSERT INTO `docker` VALUES (26,'<none>','<none>','DESKTOP-SUTF2VR','f0d3df345e1c\n'),(27,'dipfrost/essence','latest','DESKTOP-SUTF2VR','304a8c82e10a\n'),(28,'dipfrost/apollon','latest','DESKTOP-SUTF2VR','50b1446f87c0\n'),(29,'apollon','latest','DESKTOP-SUTF2VR','3414cb0864d2\n'),(30,'dipfrost/beet','latest','DESKTOP-SUTF2VR','b93899d11b4e\n'),(31,'dipfrost/olympic','latest','DESKTOP-SUTF2VR','578452fa89ce\n'),(32,'dipfrost/grav','latest','DESKTOP-SUTF2VR','76d84518da0c\n'),(33,'ubuntu','xenial','DESKTOP-SUTF2VR','5e8b97a2a082\n'),(34,'<none>','<none>','DESKTOP-SUTF2VR','ac79bcd2783b\n'),(35,'centos','latest','DESKTOP-SUTF2VR','49f7960eb7e4\n'),(36,'dipfrost/earnz','latest','DESKTOP-SUTF2VR','5d251fabd3e0\n'),(37,'dipfrost/tin','latest','DESKTOP-SUTF2VR','ef13d3ee6046\n'),(38,'dipfrost/radius','latest','DESKTOP-SUTF2VR','ca0fa0332a9d\n'),(39,'dipfrost/magna','latest','DESKTOP-SUTF2VR','1fb011422bd6\n'),(40,'dipfrost/digiwage','latest','DESKTOP-SUTF2VR','47b9d4baec50\n'),(41,'dipfrost/cmk','latest','DESKTOP-SUTF2VR','bde5cdb2e8d6\n'),(42,'dipfrost/advance','latest','DESKTOP-SUTF2VR','af63207024c8\n'),(43,'dipfrost/rover','latest','DESKTOP-SUTF2VR','165b0684e426\n'),(44,'dipfrost/green','latest','DESKTOP-SUTF2VR','b0b28832c0ef\n'),(45,'dipfrost/qbic','latest','DESKTOP-SUTF2VR','818445950973\n'),(46,'dipfrost/exus','latest','DESKTOP-SUTF2VR','d399c43f25a6\n'),(47,'dipfrost/vantaur','latest','DESKTOP-SUTF2VR','96191879b019\n'),(48,'portainer/portainer','latest','DESKTOP-SUTF2VR','50c62d83c09c\n'),(49,'zencash/gosu-base','1.10','DESKTOP-SUTF2VR','39d77870688c\n'),(50,'jenkins/jenkins','2.112-alpine','DESKTOP-SUTF2VR','481b49e06417');
/*!40000 ALTER TABLE `docker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wallets`
--

DROP TABLE IF EXISTS `wallets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wallets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `ip` varchar(150) DEFAULT NULL,
  `port` varchar(150) DEFAULT NULL,
  `blocks` varchar(255) DEFAULT NULL,
  `outputs` varchar(150) DEFAULT NULL,
  `status` varchar(150) DEFAULT NULL,
  `version` varchar(150) DEFAULT NULL,
  `timestamp` varchar(150) DEFAULT NULL,
  `locate` varchar(150) DEFAULT NULL,
  `cli` varchar(50) NOT NULL,
  `work` varchar(50) NOT NULL DEFAULT '',
  `conffile` varchar(100) NOT NULL DEFAULT '',
  `daemon` varchar(100) NOT NULL DEFAULT '',
  `confdir` varchar(100) NOT NULL DEFAULT '',
  `sentinel` int(5) NOT NULL DEFAULT '0',
  `newnode` int(11) NOT NULL DEFAULT '0',
  `dockerimage` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wallets`
--

LOCK TABLES `wallets` WRITE;
/*!40000 ALTER TABLE `wallets` DISABLE KEYS */;
INSERT INTO `wallets` VALUES (1,'Innova','192.168.1.2','14522','254052','5','6','120110','5','rivas-X299-UD4','innova-cli','','innova.conf','innovad','.innovacore',1,0,''),(2,'Advance','192.168.1.2','30889','317846','12','12','130100','5','rivas-X299-UD4','advance-cli','1','advance.conf','advanced','.advanceprotocol',1,0,'dipfrost/advance'),(3,'Tok','192.168.1.2','21118','422745','ConectionError','16','120160027',NULL,'rivas-X299-UD4','Tokugawad','0','Tokugawa.conf','Tokugawad','.Tokugawa',0,0,'dipfrost/tok'),(4,'Polis','192.168.1.2','24127','214830','1','1','1040600',NULL,'rivas-X299-UD4','polis-cli','','','','',0,0,''),(5,'Magnet','192.168.1.2','17178','413554','ConectionError','23','143061403',NULL,'rivas-X299-UD4','magnetd','','','','',0,0,''),(6,'Bltg','192.168.2.44','','','ConectionError','16','',NULL,'kiev-X299-UD4','bltg-cli','','','','',0,0,''),(7,'Magnet','192.168.2.44','','413554','ConectionError','23','143061403',NULL,'kiev-X299-UD4','magnetd','','','','',0,0,''),(8,'Earnz','192.168.2.44','','','ConectionError','10','',NULL,'kiev-X299-UD4','EarnzCoind','','','','',0,0,''),(9,'Tok','192.168.2.44','21118','422745','ConectionError','16','120160027',NULL,'kiev-X299-UD4','Tokugawad','','','','',0,0,''),(10,'Ctf','192.168.2.44','','','ConectionError','8','',NULL,'kiev-X299-UD4','coin2fly-cli','0','','','',0,0,''),(11,'Green','192.168.2.44','','','ConectionError','2','',NULL,'kiev-X299-UD4','bitcoingreen-cli','','','','',0,0,''),(12,'Crop','192.168.2.44','','','ConectionError','2','',NULL,'kiev-X299-UD4','cropcoind','','','','',0,0,''),(13,'Exus','192.168.2.44','','','ConectionError','3','',NULL,'kiev-X299-UD4','exusd','','','','',0,0,''),(14,'Apollon','192.168.2.44','','','ConectionError','1','',NULL,'kiev-X299-UD4','apollon-cli','','','','',0,0,''),(15,'Synd','192.168.2.44','','','ConectionError','1','',NULL,'kiev-X299-UD4','syndicate-cli','','','','',0,0,''),(16,'Fxtc','192.168.1.2','9469','','ConectionError','1','',NULL,'rivas-X299-UD4','fxtc-cli','1','','fxtcd','',0,0,''),(18,'Agena','192.168.1.2','','','ConectionError','','1',NULL,'rivas-X299-UD4','agena-cli','1','','','',0,0,''),(31,'Bul','192.168.2.44','',NULL,'ConectionError',NULL,NULL,NULL,'kiev-X299-UD4','bulwark-cli','','','','',0,0,''),(32,'polis','192.168.2.44','24127','214830','1','1','1040600',NULL,'kiev-X299-UD4','polis-cli','','','','',0,0,'');
/*!40000 ALTER TABLE `wallets` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-06 10:07:03
