#!/bin/bash

db='mydb'
user='userr'
pass='password'

if [ $1 = "restore" ]; then
        # Restore
        cat backup.sql | docker exec -i monitoringdocker_db_1 /usr/bin/mysql -u ${user} --password=${pass} ${db}
elif [ $1 = "backup" ]; then
        # Backup
        docker exec monitoringdocker_db_1 /usr/bin/mysqldump -u ${user} --password=${pass} ${db} > backup.sql

fi
