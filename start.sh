#!/bin/bash

git clone https://github.com/dipfrost/MonitoringSite.git
git clone https://github.com/dipfrost/MonitoringGoServer.git
cd MonitoringGoServer
docker build -t app .
cd ../
docker build -t php:fpmv2 .
docker-compose up

./db.sh restore